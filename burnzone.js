/**
 * @file
 * JavaScript for the BurnZone Drupal module.
 */

// The BurnZone global variables. Required by the embedded code.
var conversait_id = '';
var conversait_uri = '';
var conversait_title = '';
var conversait_sitename = '';

(function ($) {
  "use strict";

  /**
   * Drupal BurnZone behavior.
   */
  Drupal.behaviors.burnzone = {
    attach: function (context, settings) {
      $('body').once('burnzone', function () {
        if (settings.burnzone || false) {
          conversait_id = settings.burnzone.identifier;
          conversait_uri = settings.burnzone.url;
          conversait_title = settings.burnzone.title;
          conversait_sitename = settings.burnzone.sitename;

          // Make the AJAX call to get the BurnZone comments.
          jQuery.ajax({
            type: 'GET',
            url: '//http://cdn.theburn-zone.com/web/js/embed.js',
            dataType: 'script',
            cache: false
          });
        }

        // Load the comment numbers JavaScript.
        if (settings.burnzoneComments || false) {
          // Make the AJAX call to get the number of comments.
          window.conversait_sitename = settings.burnzoneComments;
          jQuery.ajax({
            type: 'GET',
            url: '//http://cdn.theburn-zone.com/web/js/counts.js',
            dataType: 'script',
            cache: false
          });
        }
      });
    }
  };

})(jQuery);
