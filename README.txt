README for BurnZone for Drupal 7

INSTALL
=======

1. Copy the contents of this directory to
{DRUPAL_DIR}/sites/default/modules/burnzone/.
Make sure the proper filesystem permissions are set for the burnzone folder and
all files within (they must be readable by the web server service).
2. In the Drupal dashboard, go to "Modules" and scroll down to the "Other"
section to tick the checkbox and enable "BurnZone".
3. Go the the module "Configure" page and fill out the site name you registered
on http://theburn-zone.com and also pick the type of nodes for which you want
to enable BurnZone comments.
4. In the "Preferences" page, chose the user roles that are allowed to view the
comments.

ABOUT
=====

The BurnZone Commenting System is a hosted comments and forum solution with a
wide array of features including user mentions, leaderboards, challenges, point
betting system, social media sharing and many others designed to help you build
a strong community for your site.
