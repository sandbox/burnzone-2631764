<?php

/**
 * @file
 * Admin form for BurnZone.
 */

/**
 * Menu callback; Displays the administration settings for BurnZone.
 */
function burnzone_admin_settings() {
  $form = array();
  $form['burnzone_sitename'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#description' => t('The site name that you registered BurnZone with. If you registered http://example.theburn-zone.com, you would enter "example" here.'),
    '#default_value' => variable_get('burnzone_sitename', ''),
  );
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 50,
  );
  // Visibility settings.
  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#group' => 'settings',
  );
  $types = node_type_get_types();
  $options = array();
  foreach ($types as $type) {
    $options[$type->type] = $type->name;
  }
  $form['visibility']['burnzone_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#description' => t('Apply comments to the following node types.'),
    '#default_value' => variable_get('burnzone_nodetypes', array()),
    '#options' => $options,
  );
  $form['visibility']['burnzone_location'] = array(
    '#type' => 'select',
    '#title' => t('Location'),
    '#description' => t('Display the BurnZone comments in the given location. When "Block" is selected, the comments will appear in the <a href="@burnzonecomments">BurnZone Comments block</a>.', array('@burnzonecomments' => url('admin/structure/block'))),
    '#default_value' => variable_get('burnzone_location', 'content_area'),
    '#options' => array(
      'content_area' => t('Content Area'),
      // 'block' => t('Block'),
    ),
  );
  $form['visibility']['burnzone_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#description' => t('When the comments are displayed in the content area, you can change the position at which they will be shown.'),
    '#default_value' => variable_get('burnzone_weight', 50),
    '#options' => drupal_map_assoc(array(-100, -75, -50, -25,
      0, 25, 50, 75, 100,
    )),
    '#states' => array(
      'visible' => array(
        'select[name="burnzone_location"]' => array('value' => 'content_area'),
      ),
    ),
  );

  return system_settings_form($form);
}
